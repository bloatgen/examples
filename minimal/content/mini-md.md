template: page
--
# Minimal!
This is a minimal example the template contains just a simple file. In the
content files the only absolutely required variable at the top is _template_.
This tells bloatgen which template to use to render this content.
